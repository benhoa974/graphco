<?php

class LinearAction extends CAction

{

    public function run ($id=null, $type=null,$view=null) {


    if(!@$id && isset(Yii::app()->session["userId"])){
            $id = Yii::app()->session["userId"];
            $type = Person::COLLECTION;
        }
        $controller=$this->getController();

        $itemType = Person::COLLECTION;
        if($type == "organization"){
            $itemType = Organization::COLLECTION;
        }else if($type == "event"){
            $itemType = Event::COLLECTION;
        }else if($type == "project"){
            $itemType = Project::COLLECTION;
        }

        $org = "ORGANIZATIONS";
        $proj= "PROJETS";
        $events = "EVENTS";
        $tage = "TAGS";
        
        $corg = array();
        $cproj = array();
        $cevents = array();
        $ctags = array();

        $item = PHDB::findOne( $itemType , array("_id"=>new MongoId($id)) );
       
        // $root = array( "id" => (string)$item["_id"], "label" => $item["name"], "parent" => null , "children" => 
        //             array( array("id" => "orgas", "label" => $org, "parent" => $item["name"], "children" => []),
        //                         array( "id" => "projects", "label" => $proj, "parent" => $item["name"]),
        //                         array( "id" => "events",  "label" => $events, "parent" => $item["name"]),
        //                         array( "id" => "tags",  "label" => $tags, "parent" => $item["name"]))
        //             );
        // $data = array($root); //tableau "$data" qui récupère les données de root
        

        $links = array();
        $tags = array();

        $link = "#page.type.".$itemType.".id.".(string)$item["_id"];

        if(@$item["tags"])
            foreach (@$item["tags"] as $ix => $tag) {if(!in_array($tag, $tags))$tags[] = $tag;}

        if(isset($item) && isset($item["links"])){
            if(@$item["parentId"] || @$item["organizerId"]  ){
                if(@$item["organizerId"]){
                    $item["parentId"] = $item["organizerId"];
                    $item["parentType"] = $item["organizerType"];
                }
                //ajouter une donnée dans le tableau 
                array_push($data, array( "id" => "parent", "group" => 1,  "label" => "Parent", "level" => 0 ) );
                array_push($links, array( "target" => "parent", "source" => $root["id"],  "strength" => $strength ) );
                $parent = PHDB::findOne( $item["parentType"] , array("_id"=>new MongoId( $item["parentId"] )) );
                $grp = array_search($item["parentType"], array("citoyens", "organizations", "projects", "events"))+1;
                array_push($data, array( "id" => $item["parentId"], "group" => $grp,  "label" => @$parent["name"], "level" => 2,"type"=>$item["parentType"],"tags" => @$parent["tags"], "linkSize" => count(@$parent["links"], COUNT_RECURSIVE),"img"=>@$parent["profilThumbImageUrl"] ) );
                array_push($links, array( "target" => $item["parentId"], "source" => "parent",  "strength" => $strength  ) );
            }



        foreach ($item["links"] as $key => $value){
                foreach ($value as $k => $v) {
                    // 
                    if(strcmp($key, "memberOf") == 0 || strcmp($key, "organizer") == 0){
                        $obj = Organization::getById($k);
                        if(!@$obj["_id"] || !@$obj["name"])continue;
                        
                          array_push($corg, array( "id" => (string)@$obj["_id"], "label" => @$obj["name"], "parent" => $org));  
                    }
                    //
                    else if (strcmp($key, "events") == 0){
                        $obj = Event::getById($k);
                        if(!@$obj["_id"] || !@$obj["name"])continue;
                       
                            array_push($cevents, array("id" => (string)@$obj["_id"], "label" => @$obj["name"], "parent" => $events ));
                    }
                    //
                    else if (strcmp($key, "projects") == 0){
                        $obj = Project::getById($k);
                        if(!@$obj["_id"] || !@$obj["name"])continue;
                        $obj["type"] = "projects";
                       
                            array_push($cproj, array( "id" => (string)@$obj["_id"], "label" => @$obj["name"], "parent" => $proj ));  
                    }
                    //
                    if(@$obj["tags"]){
                        foreach (@$obj["tags"] as $ix => $tag) {
                            if(!in_array($tag, $tags)){
                                $tags[] = $tag;
      
                                array_push($ctags, array( "id" => "tag".(count($tags)),  "label" => $tag, "parent" => $tage));
                            }
                        }
                    }//fin if tag
                }//fin 2eme foreach           
        }//fin 1er foreach

        $root = array( "id" => (string)$item["_id"], "label" => $item["name"], "parent" => null);
        $root["children"] = array();

        // //if(count($corg) || count($cevents) || count($cproj) || count($ctags)  ){
            
            if(count($corg) >= 1){
                $root["children"][] = array("id" => "organization", "label" => $org." (".count($corg).")" , "parent" => $item["name"], "children" =>$corg);
            }
            
            if(count($cevents)>= 1){
                $root["children"][] = array( "id" => "event", "label" => $events." (".count($cevents).")" , "parent" => $item["name"], "children" =>$cevents);
            }
            
            if(count($cproj)>= 1){
                $root["children"][] = array( "id" => "project",  "label" => $proj." (".count($cproj).")" , "parent" => $item["name"], "children" =>$cproj);
            }
            
            if(count($ctags)>= 1){
                 $root["children"][] = array( "id" => "tags",  "label" => $tage." (".count($ctags).")" , "parent" => $item["name"], "children" =>$ctags);
                }

                //var_dump($cproj);
                        
        //}
        

        // $root = array( "id" => (string)$item["_id"], "label" => $item["name"], "parent" => null , "children" =>
        //             array( 
        //                 array("id" => "organization", "label" => $org." (".count($corg).")", "parent" => $item["name"], "children" => $corg),
        //                 array( "id" => "project", "label" => $proj." (".count($cproj).")", "parent" => $item["name"], "children" => $cproj),
        //                 array( "id" => "event",  "label" => $events." (".count($cevents).")", "parent" => $item["name"], "children" => $cevents),
        //                 array( "id" => "tags",  "label" => $tage." (".count($ctags).")", "parent" => $item["name"], "children" => $ctags)
        //             )
        //         );


        // $ui = array();

        // $root = array( "id" => (string)$item["_id"], "label" => $item["name"], "parent" => null , "children" => $ui );

        //                 if(count($corg) >= 1){
        //                     $ui = array("id" => "organization", "label" => $ui, "parent" => $item["name"], "children" => $corg);

        //                 }

        //                 elseif (count($cproj) >= 1) {
        //                      $ui = array( "id" => "project", "label" => $proj." (".count($cproj).")", "parent" => $item["name"], "children" => $cproj);
                            
        //                 }

        //                 elseif ( count($cevents) >= 1) {
        //                     $ui = array( "id" => "event",  "label" => $events." (".count($cevents).")", "parent" => $item["name"], "children" => $cevents);
        //                 }
                        
        //                 // array("id" => "organization", "label" => $ui, "parent" => $item["name"], "children" => $corg),


                        
                        
        //                 // array( "id" => "project", "label" => $proj." (".count($cproj).")", "parent" => $item["name"], "children" => $cproj),
                       

                        
        //                 // array( "id" => "event",  "label" => $events." (".count($cevents).")", "parent" => $item["name"], "children" => $cevents),
                        


                        
        //                 // array( "id" => "tags",  "label" => $tage." (".count($ctags).")", "parent" => $item["name"], "children" => $ctags)

        //                 // );

                    
                
        $data =array($root);
 
        $params = array(
            'data' => $data,
            'links' => $links,
            'item' => $item,
            'tags' => $tags,
            "title" => $type." : ".$item["name"],
            "link" => $link
            );
        
        Yii::app()->theme  = "empty";
        Yii::app()->session["theme"] = "empty";

         if($view)
            Rest::json($data);
        else{
        if(Yii::app()->request->isAjaxRequest)
                $controller->renderPartial('linear', $params);
            else{
                Yii::app()->theme  = "empty";
                $controller->render('linear', $params);
                }
            }

        }
    }
}