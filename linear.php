<!DOCTYPE html>
<metacharset="utf-8">
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

  <?php
    $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile(Yii::app()->request->baseUrl. '/plugins/d3/d3.v4.min.js' );
  ?>

  <style>

    .node {
        cursor: pointer;
    }

    .node circle {
      fill: #fff;
      stroke: #2F4F4F;
      stroke-width: 3px;
    }

    .node text {
      
      font: 18px sans-serif;
    }

    .link {
      fill: none;
      stroke: #2F4F4F;
      stroke-width: 2px;
    }

    #graph {
    float: left;
    width:600px;
    }
    
    #graphtags {
    padding-top: 20px;
    background-color: #fff;
    width:0%;
    float: left;
    height:600px;
    width:20%;
    }

    #graphtags a{
    color: #333;
    text-decoration: none;
    }
    
    #sectionList a{
    color: red;
    text-decoration: none;
    }

    #search {
    float:right;
    margin-right: 100px;
    }
    
    #title {
    background-color: #eee;
    height:80px;
    font-size: 2em;
    padding:15px;
    }

    #ling{
    margin-top: 10px;
}
  </style>

       
  <div id='title'>

      <?php
          $l = $title;
            if( !empty( @$link ) )
              $l = '<a class="lbh" data-dismiss="modal" href="'.$link.'">'.$title. ' <i class="fa fa-link"></i></a>';
      ?>
    
      <div class='pull-left'><?php echo $l?></div>
      <input id='search' type='text' placeholder='#tag, free search, >types' onkeypress='return runScript(event)'/>
      <input id="ling" type=button onclick=window.location.href='http://127.0.0.1/ph/graph/co/d3/'; value=Basic />
  </div>


  <div  id="graphtags" class="hide">
    
        <div id="sectionList"></div>

  </div>  

        <a id="ui" href="http://127.0.0.1/ph/#myhome">ouiiiiiiii</a>
        <div id="box"></div>
  


  <script src="http://d3js.org/d3.v3.min.js"></script>

  <script>

    //$("#ui").click(function(event){
      //event.preventDefault();
      //.append("ce que "+ event.type + "fait, il ne veut pas")
      //.appendTo("#box");
    //});

    





    // document.querySelector("#id-checkbox").addEventListener("click", function(event) {
    //   document.getElementById("output-box").innerHTML += "non tu ne peux pas ! ";
    //   event.preventDefault();
    // }, false);

    console.log( "treeData", <?php echo json_encode($data); ?>);

    var treeData = <?php echo json_encode($data); ?>;
    var tags = <?php echo json_encode($tags); ?>;

    
   
    //console.log(treeData);

  // var Data = [
  //   {
  //     "name": "Test",
  //     "parent": "null",
  //     "children": [
  //       {
  //         "name": "ORGANIZATIONS",
  //         "parent": "Test",
  //       },
  //       {
  //         "name": "PROJETS",
  //         "parent": "Test"
  //       }
  //     ]
  //   }
  // ];
  

 
// ************** Generate the tree diagram  *****************
    
    var margin = {top: 50, right: 120, bottom: 20, left: 120},
        width = 990 - margin.right - margin.left,
        height = 750 - margin.top - margin.bottom;
        
    var i = 0,
        duration = 750,
        root;

    var tree = d3.layout.tree()
        .size([height, width]);

    var diagonal = d3.svg.diagonal()
        .projection(function(d) { return [d.y, d.x]; });

    var svg = d3.select("body").append("svg")
        .attr("width", width + margin.right + margin.left)
        .attr("height", height + margin.top + margin.bottom)
      .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    root = treeData[0];
    root.x0 = height / 2;
    root.y0 = 0;
      
    update(root);

    d3.select(self.frameElement).style("height", "500px");

    function update(source) {

      // Compute the new tree layout.
      var nodes = tree.nodes(root).reverse(),
          links = tree.links(nodes);
          console.log(links);

      console.log("nodes",nodes);

      // Normalize for fixed-depth.lien
      nodes.forEach(function(d) { d.y = d.depth * 250; });

      // Update the nodes…
      var node = svg.selectAll("g.node")
          .data(nodes, function(d) { return d.id || (d.id = ++i); });

      // Enter any new nodes at the parent's previous position.
      var nodeEnter = node.enter().append("g")
          .attr("class", "node")
          .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
          .on("click", click);

      nodeEnter.append("circle")
          .attr("r", 1e-6)
          .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

      nodeEnter.append("text")
          .attr("x", function(d) { return d.children || d._children ? -20 : 20; })
          .attr("dy", ".35em")
          .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
          .text(function(d) { return d.label; })
          .style("fill-opacity", 1e-6);

      // Transition nodes to their new position.
      var nodeUpdate = node.transition()
          .duration(duration)
          .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

      nodeUpdate.select("circle")
          .attr("r", 10)
          .style("fill", function(d) { return d._children ? "#F21C1E" :  "#21F025"; });

      nodeUpdate.select("text")
          .style("fill-opacity", 1);

      // Transition exiting nodes to the parent's new position.
      var nodeExit = node.exit().transition()
          .duration(duration)
          .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
          .remove();

      nodeExit.select("circle")
          .attr("r", 1e-6);

      nodeExit.select("text")
          .style("fill-opacity", 1e-6);

      // Update the links…
      var link = svg.selectAll("path.link")
          .data(links, function(d) { return d.target.id; });

      // Enter any new links at the parent's previous position.
      link.enter().insert("path", "g")
          .attr("class", "link")
          .attr("d", function(d) {
            var o = {x: source.x0, y: source.y0};
            return diagonal({source: o, target: o});
          });

      // Transition links to their new position.
      link.transition()
          .duration(duration)
          .attr("d", diagonal);

      // Transition exiting nodes to the parent's new position.
      link.exit().transition()
          .duration(duration)
          .attr("d", function(d) {
            var o = {x: source.x, y: source.y};
            return diagonal({source: o, target: o});
          })
          .remove();

      // Stash the old positions for transition.
      nodes.forEach(function(d) {
        d.x0 = d.x;
        d.y0 = d.y;
      });
    }

    // Toggle children on click.
    function click(d) {
      console.log("<<<<<<<<<<<<<<<<<<<<<<<",d, d.parent.id);
      //alert(d.id)
      if(d.id == "organization" || d.id == "event" || d.id == "project" || d.id == "tags")
      {
        if (d.children) {
          d._children = d.children;
          d.children = null;
        } else {
          d.children = d._children;
          d._children = null;
        }
      } else {
        open("type/"+d.parent.id+"/id/"+d.id);
        
      }
      update(d);
    
  }




  $( document ).ready(function() {
      /*$("g.node").on("click", function(event){
        event.preventDefault();
        alert();
        console.log("<<<<<<<<<<<<<<<<<<<<<<<",d);
      });*/
});


  </script>
         